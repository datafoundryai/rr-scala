package com.datafoundry

import ch.qos.logback.classic.spi.ILoggingEvent
import ch.qos.logback.core.AppenderBase
import com.typesafe.config.ConfigFactory
import net.logstash.logback.encoder.LogstashEncoder
import scalaj.http.Http
import scala.concurrent.{ExecutionContext, Future}

/**
 * @author sasikala
 * @created on 12/16/2020
 * @description custom http appender for publishing log events to http service
 * @version number v1.0
 */
class HttpAppender extends AppenderBase[ILoggingEvent] {
  implicit val executionContext = ExecutionContext.global
  val config = ConfigFactory.load()
  var encoder: LogstashEncoder = null

  override def append(eventObject: ILoggingEvent): Unit = {
    if(encoder!=null){
      val logEvent = new String(encoder.encode(eventObject))
      publishMessage(logEvent)
    }
  }

  def publishMessage(message: String) = {
    val dpaService = config.getString("dpaService.baseUrl") + config.getString("dpaService.path")
    /* asynchronous processing of publishing log events to dpa-service */
    Future {
      Http(dpaService)
        .postData(message)
        .header("content-type", "application/json").asString
    }
  }

  def getEncoder: LogstashEncoder = encoder

  def setEncoder(encoder: LogstashEncoder): Unit = {
    this.encoder = encoder
  }
}

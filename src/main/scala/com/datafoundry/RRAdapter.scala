package com.datafoundry

/**
 * @author sasikala
 * @created on 11/25/2020
 * @description Trait for wrapper class for structured logging
 * @version number v1.0
 */
trait RRAdapter {
  def info(rrId: String, source: String, message: String, optionalParam: Map[String, String])
  def warn(rrId: String, source: String, message: String, optionalParam: Map[String, String])
  def debug(rrId: String, source: String, message: String, optionalParam: Map[String, String])
  def error(rrId: String, source: String, message: String, optionalParam: Map[String, String])
}

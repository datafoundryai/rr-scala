import com.datafoundry.{RRAdapter, RRAdapterImpl}

/**
 * @author sasikala
 * @created on 11/25/2020
 * @description class for testing log wrapper implementation
 * @version number v1.0
 */
object Main extends App {
  val logger: RRAdapter = new RRAdapterImpl(getClass.getName)
  val additionalParam: Map[String, String] = Map[String, String]("requestingService" -> "ui-service", "solutionId" -> "1234")
  logger.info("123", "cvcm", "Message published to kafka: pvci_ocr_output1", additionalParam)
}
